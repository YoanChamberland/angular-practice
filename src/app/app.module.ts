import { AppComponent } from './app.component';
import { AppareilComponent } from './appareil/appareil.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MonPremierComponent } from './mon-premier/mon-premier.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [AppComponent, MonPremierComponent, AppareilComponent],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
