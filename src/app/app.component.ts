import { AppareilComponent } from './appareil/appareil.component';
import { Component } from '@angular/core';

@Component({
  providers: [AppareilComponent],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth = true;
  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(() => {
      resolve(date);
    }, 2000);
  });
  appareils = [
    {
      name: 'Ordi',
      status: 'allumé'
    },
    {
      name: 'portable',
      status: 'éteint'
    }
  ];
  onAllumer() {
    if (this.isAuth) {
      this.isAuth = false;
    } else {
      this.isAuth = true;
    }
  }
}
